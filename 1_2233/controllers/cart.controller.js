const db = require('../db');

module.exports.addToCart = (req, res, next) =>{
    var productId = req.params.productId;
    var sessionId = req.signedCookies.sessionId;
    
    if(!sessionId) {
        res.redirect('/products');
        return;
    }

    /*var count = db.get('sessions')
    .find({ id: sessionId })
    .get('cart.' + productId, 0)
    .value();

    db.get('sessions')
    .find({ id: sessionId })
    .set('cart.' + productId, count + 1)
    .write();*/

    var temp = db.get('sessions')
    .find({ id: sessionId });
    
    temp.get('cart').push({ name: productId, num: 0})
    .write();

    res.redirect('/products');
}

module.exports.index = (req, res) => {res.render('cart/index',{
        sessions: db.get('sessions').value()
    });
};