const express = require('express');
const router = express.Router();
const controller = require('../controllers/cart.controller');
//const validate = require('../validate/user.validate');

router.get('/add/:productId', controller.addToCart);

router.get('/', controller.index);

module.exports = router;