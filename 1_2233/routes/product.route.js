const express = require('express');
const router = express.Router();
const controller = require('../controllers/product.controller');
const authMiddleware = require('../middlewares/auth.middleware');

//router.get('/', authMiddleware.requireAuth, controller.index);
router.get('/', controller.index);
module.exports = router;